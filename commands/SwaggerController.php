<?php

namespace app\commands;

use yii\console\Controller;
use Yii;
use yii\console\ExitCode;
use yii\helpers\Console;
use function OpenApi\scan;


/**
 * Скрипт актуализации swagger.yaml
 */
class SwaggerController extends Controller {

    public function actionIndex()
    {
        define('EXAMPLE_SERVER_URL', 'http://wrk');
             
        $openApi = scan(Yii::getAlias('@app/modules/api/controllers'));
        $file = Yii::getAlias('@app/web/swagger/swagger.yaml');

        $handle = fopen($file, 'wb');
        fwrite($handle, $openApi->toYaml());
        fclose($handle);

        echo $this->ansiFormat('Created', Console::FG_BLUE);

        return ExitCode::OK;
    }
}
