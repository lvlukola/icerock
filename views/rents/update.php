<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\rents */

$this->title = 'Изменить данные аренды: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Аренда', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="rents-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'motorcycles' => $motorcycles
    ]) ?>

</div>
