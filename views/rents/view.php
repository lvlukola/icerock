<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\rents */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Аренда', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="rents-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Уверены, что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            [                      // the owner name of the model
                'label' => 'Мотоцикл',
                'value' => function ($model){
                    $motoModel = $model->moto->model;
                    $motoColor = $model->moto->color;
                    return "$motoModel ($motoColor)";
                },
            ],
            ['attribute' => 'date_from', 'format' => ['datetime', 'php:Y-m-d H:i:s']],
            ['attribute' => 'date_to', 'format' => ['datetime', 'php:Y-m-d H:i:s']],
        ],
    ]) ?>

</div>
