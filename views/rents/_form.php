<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\rents */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rents-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'motorcycle')->dropDownList(
            /*array_map(function($row) {
                return [$row['id'] => $row['id']];
            }, $motorcycles)*/
        ArrayHelper::map($motorcycles, 'id', 'color', 'model' ),
        [
            'prompt' => 'Выберите мотоцикл',
        ]
    ) ?>
    
    <?= $form->field($model, 'date_from')->textInput([
            'value' => Yii::$app->formatter->asDatetime($model->date_from ?? new DateTime(), 'php:Y-m-d H:i:sO'),
        ])
    ?>
    
    <?= $form->field($model, 'date_to')->textInput([
            'value' => Yii::$app->formatter->asDatetime($model->date_to ?? new DateTime(), 'php:Y-m-d H:i:sO'),
        ])
    ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
