<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\rents */

$this->title = 'Добавить аренду';
$this->params['breadcrumbs'][] = ['label' => 'Аренда', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rents-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'motorcycles' => $motorcycles
    ]) ?>

</div>
