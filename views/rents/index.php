<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Аренда';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rents-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'username',
            [
                'header' => 'Мотоцикл',
                'value' => function ($model){
                    $motoModel = $model->moto->model;
                    $motoColor = $model->moto->color;
                    return "$motoModel ($motoColor)";
                },
            ],
            ['attribute' => 'date_from', 'format' => ['datetime', 'php:Y-m-d H:i:s']],
            ['attribute' => 'date_to', 'format' => ['datetime', 'php:Y-m-d H:i:s']],
            
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
