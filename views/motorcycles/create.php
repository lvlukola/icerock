<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\motorcycles */

$this->title = 'Добавить мотоцикл';
$this->params['breadcrumbs'][] = ['label' => 'Мотоциклы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="motorcycles-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
