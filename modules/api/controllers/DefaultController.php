<?php

namespace app\modules\api\controllers;

use \yii\rest\Controller;
use app\models\Motorcycles;
use app\models\Rents;
use Yii;

class DefaultController extends Controller
{
    /**
     * Список мотоциклов
     * @param string $searchText
     * @param int $start
     * @param int $limit
     * @return array
     */
    public function actionList(string $searchText = null, int $start = 0, int $limit = 10): array
    {
        $motoList = Motorcycles::find()
                ->filterWhere(['OR',
                        ['ilike', 'model', $searchText],
                        ['ilike', 'color', $searchText],
                    ])
                ->offset($start)
                ->limit($limit)
                ->asArray()
                ->all();
        
        return [
                'status' => 'ok',
                'items' => $motoList ?? []
            ];
    }
    
    /**
     * Списо аренд мотоцикла
     * @param string $id
     * @return array
     */
    public function actionDetails(string $id): array
    {
        $rents = Rents::find()
                ->where(['motorcycle' => $id])
                ->asArray()
                ->all();
                
        return [
                'status' => 'ok',
                'items' => $rents ?? []
            ];
    }
    
    /**
     * Аоендовать мотоцикл
     * @param string $id
     * @return array
     */
    public function actionAdd(string $id): array
    {
        $rent = new Rents();
        
        $rent->motorcycle = $id;
        if ($rent->load(Yii::$app->request->post(), '') && $rent->save()) {
            $result = ['status' => 'ok'];
        } else {
            $result = [
                    'status' => 'error',
                    'errors' => $rent->getErrors()
                ];
        }
        
        return $result;
    }
    
    /**
     * @OA\Info(title="IceRockApi", version="0.1")
     */
    
    /**
     * @OA\GET(
     *   path="/api/moto",
     *   tags={"moto"},
     *   summary="Список",
     *   @OA\Parameter(ref="#/components/parameters/search_in_query"),
     *   @OA\Parameter(ref="#/components/parameters/start_in_query"),
     *   @OA\Parameter(ref="#/components/parameters/limit_in_query"),
     *   @OA\Response(response=200,ref="#/components/responses/200"),
     * )
     */
    
    /**
     * @OA\GET(
     *   path="/api/moto/{id}",
     *   tags={"moto"},
     *   summary="Детальная информация",
     *   @OA\Parameter(ref="#/components/parameters/id_in_path_required"),
     *   @OA\Response(response=200,ref="#/components/responses/200"),
     * )
     */
    
    /**
     * @OA\POST(
     *   path="/api/moto/{id}/rent",
     *   tags={"moto"},
     *   summary="Аренда",
     *   @OA\Parameter(ref="#/components/parameters/id_in_path_required"),
     *   @OA\RequestBody(
     *     required=true,
     *     @OA\MediaType(
     *       mediaType="application/json",
             @OA\Schema(ref="#/components/schemas/scheme"),
     *     ),
     *   ),
     *   @OA\Response(response=200,ref="#/components/responses/200"),
     * )
     */
    
    /**
     *  @OA\Schema(
     *      schema="scheme",
     *      required={"dateFrom", "dateTo"},
     *      @OA\Property(property="dateFrom", type="varchar"),
     *      @OA\Property(property="dateTo", type="varchar"),
     *      example={
     *          "username": "test_user",
     *          "date_from": "2021-10-01 12:00:00+0300",
     *          "date_to": "2021-10-01 12:00:01+0300",
     *      }
     *  ),
     */
    
    /**
     * @OA\Parameter(
     *   parameter="id_in_path_required",
     *   name="id",
     *   description="ID записи",
     *   @OA\Schema(
     *      type="string",
     *   ),
     *   in="path",
     *   required=true
     * )
     */
    
    /** @OA\Parameter(
     *   parameter="start_in_query",
     *   name="start",
     *   description="Выводить записи с ...",
     *   @OA\Schema(
     *      type="integer",
     *   ),
     *   in="query"
     * )
     */
    
    /** @OA\Parameter(
     *   parameter="limit_in_query",
     *   name="limit",
     *   description="Количество возвращаемых записей",
     *   @OA\Schema(
     *      type="integer",
     *   ),
     *   in="query"
     * )
     */
    
    /** @OA\Parameter(
     *   parameter="search_in_query",
     *   name="searchText",
     *   description="Поиск по полям",
     *   @OA\Schema(
     *      type="string",
     *   ),
     *   in="query"
     * )
     */
    
    /** 
     * @OA\Response(
     *   response="200", 
     *   description="Успешно",
     *   @OA\MediaType(
     *      mediaType="application/json",
     *      @OA\Schema(
     *          required={"status"},
     *          @OA\Property(
     *              property="status", 
     *              type="string"),
     *          @OA\Property(
     *              property="items", 
     *              type="object"),
     *          @OA\Property(
     *              property="errors", 
     *              type="object"),
     *          example={
     *              "status": "ok/error",
     *              "items": {
     *                  "color": "red",
     *                  "model": "Minsk",
     *              },
     *              "errors": {
     *                 "id": "Значение «c35464f5-18d9-407a-97b8-b8c73b65e23a» для «ID» уже занято."
     *              }
     *          }),
     *   ),
     * )
     */
}
