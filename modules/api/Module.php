<?php

namespace app\modules\api;
use Yii;

/**
 * api module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\api\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        
        Yii::$app->request->parsers = [
                'application/json' => 'yii\web\JsonParser',
            ];
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    }
}
