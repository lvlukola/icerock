<?php

use yii\db\Migration;

/**
 * Class m211028_152842_init
 */
class m211028_152842_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->execute('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
        
        $this->createTable('motorcycles', [
            'id'      => 'UUID PRIMARY KEY not null default uuid_generate_v4()',
            'model'   => 'varchar(100)',
            'color'   => 'varchar(100)',
        ]);

        $this->addCommentOnTable('motorcycles', 'Мотоциклы');

        $this->addCommentOnColumn('motorcycles', 'model', 'Модель строкой (правильнее ссылкой на другую таблицу с моделями)');
        $this->addCommentOnColumn('motorcycles', 'color', 'Цвет строкой');
        
        
        $this->createTable('rents', [
            'id'         => 'UUID PRIMARY KEY not null default uuid_generate_v4()',
            'username'   => 'varchar(100) not null',
            'motorcycle' => 'UUID not null REFERENCES "motorcycles"("id") ON UPDATE CASCADE ON DELETE RESTRICT',
            'date_from'  => 'timestamptz not null',
            'date_to'    => 'timestamptz not null'
,       ]);
        
        $this->addCommentOnColumn('rents', 'username', 'Имя пользователя');
        $this->addCommentOnColumn('rents', 'motorcycle', 'Мотоцикл');
        $this->addCommentOnColumn('rents', 'date_from', 'Начало аренды');
        $this->addCommentOnColumn('rents', 'date_to', 'Окончание аренды');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('rents');
        $this->dropTable('motorcycles');
    }
}
