<?php

namespace app\models;
use Yii;

/**
 * This is the model class for table "rents".
 *
 * @property string $id
 * @property string $username Имя пользователя
 * @property string $motorcycle Мотоцикл
 * @property string $date_from Начало аренды
 * @property string $date_to Окончание аренды
 *
 * @property Motorcycles $motorcycle0
 */
class Rents extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rents';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'motorcycle', 'date_from', 'date_to'], 'required'],
            [['id', 'motorcycle'], 'string'],
            [['date_from', 'date_to'], 'datetime', 'format' => 'php:Y-m-d H:i:sO', 'strictDateFormat' => true],
            [['username'], 'string', 'max' => 100],
            [['id'], 'unique'],
            [['motorcycle'], 'exist', 'skipOnError' => true, 'targetClass' => Motorcycles::className(), 'targetAttribute' => ['motorcycle' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Пользователь',
            'motorcycle' => 'Мотоцикл',
            'date_from' => 'Начало аренды',
            'date_to' => 'Окончание аренды',
        ];
    }
    
    public function afterValidate() {
        parent::afterValidate();
        
        if ($this->hasErrors() || $this->datesIsNotValid()) {
            return;
        }

        $rents = self::find()
                ->where(['motorcycle' => $this->motorcycle])
                ->andWhere(['AND',
                    ['<=', 'date_from', $this->date_to],
                    ['>=', 'date_to', $this->date_from],
                ]);
        
        if ($this->id) {
            $rents->andWhere(['<>', 'id', $this->id]);
        }
        
        if ($rents->exists()) {
            $this->addError('motorcycle', 'Мотоцикл арендован на данный период');
        }
    }
    
    /**
     * Проверка дат
     * @return boolean
     */
    private function datesIsNotValid() {
        $dateFrom = strtotime($this->date_from);
        $dateTo = strtotime($this->date_to);
        
        if ($dateFrom >= $dateTo) {
            $this->addError('date_to', 'Время окончания аренды должно быть больше времени начала');
            return true;
        }
        
        return false;
    }
    
    /**
     * Gets query for [[Motorcycle]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMoto()
    {
        return $this->hasOne(Motorcycles::className(), ['id' => 'motorcycle']);
    }
}
