<?php

namespace app\models;

use Yii;
use yii\db\Exception;

/**
 * This is the model class for table "motorcycles".
 *
 * @property string $id
 * @property string|null $model Модель строкой (правильнее ссылкой на другую таблицу с моделями)
 * @property string|null $color Цвет строкой
 *
 * @property Rents[] $rents
 */
class Motorcycles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'motorcycles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'string'],
            [['model', 'color'], 'string', 'max' => 100],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model' => 'Модель',
            'color' => 'Цвет',
        ];
    }
    
    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        if ($this->getRents()->count()) {
            throw new Exception('Невозможно удалить мотоцикл у которого есть аренда');
        }
        
        return true;
    }

    /**
     * Gets query for [[Rents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRents()
    {
        return $this->hasMany(Rents::className(), ['motorcycle' => 'id']);
    }
}
